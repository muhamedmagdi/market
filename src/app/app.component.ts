import { Component } from '@angular/core';
import { UsersService } from './users.service';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { UserApp } from './modle/user-model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  constructor( private usersaved: UsersService , public auth: AuthService , public router: Router) {
   
   auth.user$.subscribe(user => {
      if ( user) { 
        this.usersaved.save(user) ;
        const returnedUrl = localStorage.getItem('returnedUrl') ;
        this.router.navigateByUrl(returnedUrl) ;

       }
    }) ;
  }
}
