import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor( public db: AngularFireDatabase) { }
  saveData(product) {
    return this.db.list('/products-list/').push(product) ;
  }
  getAll() {
    return this.db.list('/products-list/').snapshotChanges();
  }
  getProduct(id) {
    return this.db.object('/products-list/' + id) ;
  }
  update(prductId , product ) {
    return this.db.object('/products-list/' + prductId  ).update(product) ;
  }
  delete(id) {
   return this.db.object('/products-list/' + id).remove() ;
  }
}
