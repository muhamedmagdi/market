import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { UserList } from '../admin/admin-products/admin-products.component';
import { CategoryService } from '../category.service' ;
import {ActivatedRoute} from '@angular/router'
import { ShoppingCardService } from '../shopping-card.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
products = [] ;
list ;
categories = [];
  filteredCategory=[];
  categor;
  constructor(public proser: ProductService ,
     public category: CategoryService , 
     public route : ActivatedRoute ,
     private cartServ : ShoppingCardService) { 
    
  }

  ngOnInit() {
    this.list =  this.proser.getAll();
    this.list.subscribe(item =>  {
       item.forEach(a=> {
         let item = a.payload.toJSON() ;
         item['$key'] = a.key ;
      this.products.push( item as UserList )    ;
      this.products = this.filteredCategory ;
     }) ;
     })
     this.route.queryParamMap.subscribe(params=> {
      this.categor = params.get('category') ;
      console.log(this.categor) ;
      this.filteredCategory = (this.categor)? this.products.filter(p=>{
        console.log(p) ;
       p.category === this.categor ;
      }):this.products
       }) ;
     this.category.getCategory().subscribe(cat => {
       cat.forEach(a=> {
        let item = a.payload.toJSON() ;
        item['$key'] = a.key ;
     this.categories.push( item as Category )    ;
      })
    })

  }

  addToCart(product) {
     this.cartServ.addTocart(product) ;
  }
}
export class Category {
  $key : string ;
  name: string
}
