
import { AngularFireDatabase, AngularFireObject  } from 'angularfire2/database';
import * as firebase from 'firebase' ;
import { UserApp } from './modle/user-model';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})

export class UsersService {

  constructor( private db: AngularFireDatabase) { }
   save(user: firebase.User) {
      this.db.object('/users/'+user.uid).update({
    name: user.displayName ,
    email : user.email
  });
  }

  getUser(uid: string) : AngularFireObject<UserApp> {
    return this.db.object('/users/'+ uid) ;
  }

}
