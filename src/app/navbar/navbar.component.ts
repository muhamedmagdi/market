import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { UserApp } from '../modle/user-model';
@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  appUser: UserApp;
  constructor( public auth: AuthService ) {
    this.auth.appUser$.subscribe(UserApp => this.appUser = UserApp) ;
  }
  logout() {
  this.auth.logout() ;
  }
}
