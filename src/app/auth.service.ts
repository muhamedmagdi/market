
import { AngularFireAuth } from 'angularfire2/auth' ;
import * as firebase from 'firebase/app' ;
import { Observable } from 'rxjs';
import { UsersService } from './users.service';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserApp } from './modle/user-model';
import {switchMap} from 'rxjs/operators';
import { of } from 'rxjs';
import { AngularFireObject } from 'angularfire2/database';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable <firebase.User> ;
  constructor( public afAuth: AngularFireAuth , private userservice: UsersService , private  route: ActivatedRoute) {
   this.user$ = afAuth.authState ;
  }
  login() {
   let  returnedUrl = this.route.snapshot.queryParamMap.get('retunUrl') ;
    localStorage.setItem('returnedUrl' ,  returnedUrl) ;
    this.afAuth.auth.signInWithRedirect( new firebase.auth.GoogleAuthProvider()) ;
   }
   logout()  {
    this.afAuth.auth.signOut() ;
  }
  get appUser$() : Observable<UserApp> {
   return  this.user$.pipe(switchMap((user) =>{ if(user) return this.userservice.getUser(user.uid).valueChanges() ;
  return of(null) ;
  }));
  }
}
