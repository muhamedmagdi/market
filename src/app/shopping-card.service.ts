import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { take } from 'rxjs/operators';
import { UserList } from './admin/admin-products/admin-products.component';
import { Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCardService {

  constructor( private db: AngularFireDatabase) {}
 public async getCart() {
  const cartId = await this.checkCart() ;
 
  return await this.db.object('/shopping-card/' + cartId).snapshotChanges() ;
  }

  private async checkCart(){
    const cartId = localStorage.getItem('cartId') ;
     if (cartId) {return cartId ; };

     const result = await  this.addCart() ;
        localStorage.setItem('cartId' , result.key) ;

     return result.key ;
  }
 private async addCart() {
    return await this.db.list('/shopping-card/').push({
      timeCreated : new Date().getTime()
    }) ;
  }
  CartItem(cartId : string ,productId : string) {
  
return    this.db.object<any>('/shopping-card/' + cartId + '/items/' + productId ) ;
}
async addTocart(product :UserList) {
  const CartId = await this.checkCart() ;
  const c = CartId ;
  const p = product.$key ;
  const items$ =  await this.CartItem(c , p) ;
  console.log(c) ;
  console.log(p) ;
  items$.snapshotChanges().pipe(take(1)).subscribe( (item :any) => {
   console.log(item)

    if (item.key != null) {
      const i = item.payload.val().quantity  ;
      console.log(i)
      items$.update( { 
       quantity : i + 1
       
} 
) 
    }
    else  {
       items$.update( { 
         
         title: product.title,
         price: product.price,
        imageUrl: product.imageUrl,
        category : product.category ,
        quantity :   1
        
 }); 
      }
 });
}
}
