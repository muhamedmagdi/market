import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import {RouterModule } from '@angular/router' ;
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ShoppingCardComponent } from './shopping-card/shopping-card.component';
import { ChechOutComponent } from './chech-out/chech-out.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { from } from 'rxjs';
import { LoginComponent } from './login/login.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { AdminOrdersComponent } from './admin/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { LogoutComponent } from './admin/logout/logout.component';
import { AuthService } from './auth.service';
import { UsersService } from './users.service';
import { GuardService } from './guard.service';
import { AdminAuthService} from './admin-auth.service' ;
import { ProductFormComponent } from './admin/product-form/product-form.component';
import { CategoryService } from './category.service';
import { FormsModule} from '@angular/forms' ;
import { CustomFormsModule } from 'ng2-validation'


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProductsComponent,
    ShoppingCardComponent,
    ChechOutComponent,
    OrderSuccessComponent,
    MyOrdersComponent ,
    LoginComponent ,
    AdminOrdersComponent ,
    AdminProductsComponent,
    LogoutComponent,
    ProductFormComponent ,
  ],
  imports: [
    BrowserModule,
    FormsModule ,
    CustomFormsModule ,
    AngularFireDatabaseModule ,
    AngularFireAuthModule  ,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule.forRoot([
      {path : '' , component : HomeComponent} ,
      {path : 'shopping-card' , component : ShoppingCardComponent} ,
      {path : 'products' , component : ProductsComponent, canActivate: [GuardService]} ,
      {path : 'check-out' , component : ChechOutComponent , canActivate: [GuardService]  } ,


      {path : 'order-success' , component :  OrderSuccessComponent , canActivate: [GuardService]} ,
      {path : 'my-order' , component : MyOrdersComponent , canActivate: [GuardService]  } ,
      {path : 'login' , component : LoginComponent} ,
      {path : 'admin-orders' , component : AdminOrdersComponent ,canActivate: [GuardService , AdminAuthService]} ,
      {path : 'admin-products' , 
      component : AdminProductsComponent ,
      canActivate: [GuardService , AdminAuthService]} ,
      {path : 'add-product' , component : ProductFormComponent ,pathMatch: 'full',
      canActivate: [GuardService , AdminAuthService]} ,
      {path : 'add-product/:id' , component : ProductFormComponent ,pathMatch: 'full',
      canActivate: [GuardService , AdminAuthService]} ,
      {path : 'logout' , component : LogoutComponent}
    ])
  ],
  providers: [AuthService , UsersService , GuardService , CategoryService , AdminAuthService , ShoppingCardComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
