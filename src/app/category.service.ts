import { Injectable } from '@angular/core';
import { AngularFireDatabase  } from 'angularfire2/database';
import { __values } from 'tslib';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(public db: AngularFireDatabase) { }
  getCategory() {
   return this.db.list('/categories/').snapshotChanges();
  }
  }

