import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { map , switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UserApp} from './modle/user-model' ;
import { UsersService } from './users.service';
@Injectable({
  providedIn: 'root'
})
export class AdminAuthService implements CanActivate {

  constructor( public auth: AuthService , public userser : UsersService ) { }
   canActivate() : Observable<boolean> {
     return  this.auth.appUser$
    .pipe(map((UserApp ) => UserApp.isAdmin)) ;
  }
}
