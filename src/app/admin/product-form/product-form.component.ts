import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/category.service';
import { ProductService } from 'src/app/product.service';
import { Router , ActivatedRoute } from '@angular/router';
import 'rxjs/operators';
import { Category } from 'src/app/home/home.component';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
 categories$ =[];
 product={} ;
 id;
  constructor( public categoryservice: CategoryService ,
     public productser: ProductService ,
     public router: Router ,
     public route: ActivatedRoute ) {
    categoryservice.getCategory().subscribe(cat=> {
      cat.forEach(a=> {
        let item = a.payload.toJSON() ;
        item['$key'] = a.key ;
     this.categories$.push( item as Category )    ;
     
      })
    });
    this.id = this.route.snapshot.paramMap.get('id') ;
   if (this.id) {this.productser.getProduct(this.id).valueChanges().subscribe( p => this.product = p) ; }
   }
   save(product) {
    if (this.id) { this.productser.update(this.id , product) ;
     } else { this.productser.saveData(product); }
    this.router.navigate(['/admin-products']) ;
   }

  ngOnInit() {
  }

}
