import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from 'src/app/product.service';
import { Observable, Subscription } from 'rxjs';
import { AngularFireList } from 'angularfire2/database';
import { UserApp } from 'src/app/modle/user-model';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit  {
  items$=[] ;
  list ;
  filteredList;
  subscribtion: Subscription;
  constructor(public proser: ProductService) {
    
    }


  ngOnInit() {
    this.list =  this.proser.getAll();
    this.list.subscribe(item =>  {
       item.forEach(a=> {
         let item = a.payload.toJSON() ;
         item['$key'] = a.key ;
      this.items$.push( item as UserList )    ;
      
       })
     }) ;
  }

  Delete(id) {
    this.proser.delete(id) ;
    let index = this.items$.indexOf(id) ;
   const  updatedList = this.items$.splice(0,index);
    this.items$ = updatedList ;
    
  }
  search(word : string) {
    console.log(word) ;
    this.filteredList = this.items$.filter(p => p.title.toLowerCase().includes(word.toLowerCase()));
        if (word) {
           this.items$ =this.filteredList ;
        }else{
          this.items$ ;
        }

}
}
export class UserList {
  $key: string;
  title:string ;
  price: string ;
  category : string ;
  imageUrl : string ;

}