// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false ,
  firebase: {
    apiKey: 'AIzaSyCSPbBlo1s-4PygxtSdlRQPWGlIifO1y0w',
    authDomain: 'market-f65eb.firebaseapp.com',
    databaseURL: 'https://market-f65eb.firebaseio.com',
    projectId: 'market-f65eb',
    storageBucket: 'market-f65eb.appspot.com',
    messagingSenderId: '766366378478',
    appId: '1:766366378478:web:17c33f096d156cbe'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
